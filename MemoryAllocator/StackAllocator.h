#pragma once

class StackAllocator
{
public:
	StackAllocator(void* start, void* end) : m_start((char*)start), m_end((char*)end) {
		m_current = m_start;
	};
	void* Allocate(size_t size, size_t alignment, size_t offset);
	void Free(void* ptr);
private:
	char* m_start;
	char* m_end;
	char* m_current;
};