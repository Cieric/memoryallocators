#pragma once
#include "ArchUtils.h"
#include <Windows.h>
#include <intrin.h>
#include <stdint.h>
#include <assert.h>

template<typename T, size_t pages = 1>
class VirtualObjectPoolAllocator
{
	const size_t pageSize;
	const size_t elemSize;
	const size_t elemPerPage;
	const size_t dataOffset;
	char* head = nullptr;

	///sizeof(PageInfo) + sizeof(T) * elemPerPage < pageSize;
	//struct PageInfo
	//{
	//	PageInfo* next;
	//	size_t free;
	//	size_t used;
	//	char[?] usedBlocks;
	//};

	__inline void* AllocNewPage()
	{
		char* table = (char*)VirtualAlloc(nullptr, pageSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
		//void* base = table+(2 * sizeof(size_t) + elemPerPage / 8);
		*(void**)(&table[0]) = nullptr;
		*(size_t*)(&table[sizeof(void*)]) = elemPerPage;
		*(size_t*)(&table[sizeof(size_t) + sizeof(void*)]) = 0;
		memset(&table[sizeof(size_t) * 2 + sizeof(void*)], 0, elemPerPage / 8);
		if (elemPerPage % 8 != 0)
			(*(uint8_t*)&table[sizeof(size_t) * 2 + sizeof(void*) + elemPerPage / 8]) = (1 << (8 - (elemPerPage % 8))) -1;
		return table;
	}

	VirtualObjectPoolAllocator() :
		pageSize(ArchUtils::GetPageSize()*pages),
		elemSize(sizeof(T)),
		elemPerPage((8 * (pageSize - 2 * sizeof(size_t) - sizeof(void*))) / (8 * sizeof(T) + 1)),
		dataOffset(2 * sizeof(size_t) + sizeof(void*) + (elemPerPage+7)/8)
	{
		head = (char*)AllocNewPage();
	}

	uint32_t __inline msb(unsigned char value)
	{
		DWORD leading_zero = 0;
		unsigned long temp = (unsigned char)~value;
		if (_BitScanReverse(&leading_zero, temp))
			return leading_zero;
		return -1;
	}

	__inline void SetBit(char* block, size_t idx, bool value)
	{
		char v = idx % 8;
		if (value)
			block[idx / 8] |= (1 << (7 - v));
		else
			block[idx / 8] &= ~(1 << (7 - v));
	}

	__inline size_t GetFree(char* block)
	{
		char* usedBlocks = &block[sizeof(size_t) * 2 + sizeof(void*)];
		size_t i = 0;
		size_t count = elemPerPage / 8 + ((elemPerPage % 8) ? 1 : 0);
		for (; i < count; i++)
		{
			if ((unsigned char)usedBlocks[i] != 0xFF)
			{
				return (7 - msb(usedBlocks[i])) + i * 8;
			}
		}
		return -1;
	}

	__inline bool GetBit(char* block, size_t idx)
	{
		return block[idx / 8] & (1 << (7 - (idx % 8)));
	}

	__inline size_t GetFree(char* block, size_t wanted)
	{
		char* usedBlocks = &block[sizeof(size_t) * 2 + sizeof(void*)];
		size_t i = 0;
		size_t count = 0;
		for (; i < elemPerPage; i++)
		{
			if (count == wanted)
				return i - count;
			if (GetBit(usedBlocks, i) == 0)
				count += 1;
			else
				count = 0;
		}
		return -1;
	}

public:
	static VirtualObjectPoolAllocator<T,pages>* Get()
	{
		static VirtualObjectPoolAllocator<T,pages>* singleton = nullptr;
		if (singleton == nullptr)
			singleton = new VirtualObjectPoolAllocator<T,pages>();
		return singleton;
	}

	T* Alloc()
	{
		char* next = head;
		while (next != nullptr)
		{
			if (*(size_t*)(&next[sizeof(void*)]) > 0)
			{
				size_t offset = GetFree(next);
				SetBit(next + sizeof(size_t) * 2 + sizeof(void*), offset, true);
				*(size_t*)(next + sizeof(void*)) -= 1;
				*(size_t*)(next + sizeof(void*) + sizeof(size_t)) += 1;
				return &((T*)(next + dataOffset))[offset];
			}
			if (*(char**)next == nullptr)
				*(char**)next = (char*)AllocNewPage();
			next = *(char**)next;
		}
		return nullptr;
	};

	T* Alloc(size_t count)
	{
		assert(count <= elemPerPage);
		assert(count > 0);
		char* next = head;
		while (next != nullptr)
		{
			if (*(size_t*)(&next[sizeof(void*)]) >= count)
			{
				size_t offset = GetFree(next, count);
				if (offset == -1)
					goto nextPage;

				for(int i=0; i<count; i++)
					SetBit(next + sizeof(size_t) * 2 + sizeof(void*), offset+i, true);

				*(size_t*)(next + sizeof(void*)) -= count;
				*(size_t*)(next + sizeof(void*) + sizeof(size_t)) += count;
				return &((T*)(next + dataOffset))[offset];
			}
		nextPage:
			if (*(char**)next == nullptr)
				*(char**)next = (char*)AllocNewPage();
			next = *(char**)next;
		}
		return nullptr;
	};
	
	void Free(T* v)
	{
		char* Ptable = (char*)(((intptr_t)v / pageSize)*pageSize);
		char* Pdata = (char*)(Ptable + dataOffset);
		*(size_t*)(Ptable + sizeof(void*)) += 1;
		*(size_t*)(Ptable + sizeof(void*) + sizeof(size_t)) -= 1;
		size_t elemOffset = (v - (T*)Pdata) / sizeof(T);
		SetBit(Ptable + sizeof(size_t) * 2 + sizeof(void*), elemOffset, false);
	}
};

