#include <iostream>
#include <Windows.h>
#include <intrin.h>
#include "StackAllocator.h"
#include "VirtualAllocator.h"
#include <vector>

#define KB 1024
#define MB KB*1024
#define GB MB*1024

int main(int argc, char** argv)
{
	VirtualObjectPoolAllocator<double, 20>* allocatord = VirtualObjectPoolAllocator<double, 20>::Get();
	std::vector<double*> blah;
	double* tempArr = allocatord->Alloc(20);
	for (int i = 0; i < 20; i++)
		tempArr[i] = 3.141592653589;

	for (int i = 0; i < 512; i++)
	{
		double* temp = allocatord->Alloc();
		*temp = 3.141592653589;
		blah.push_back(temp);
	}
	getchar();
}