#pragma once
#include <stdint.h>

namespace ArchUtils
{
	size_t GetPageSize(void);
	uint64_t GetTotalRam(void);
}