#include "ArchUtils.h"
#include <Windows.h>

size_t ArchUtils::GetPageSize(void)
{
	static size_t size = -1;
	if (size == -1)
	{
		SYSTEM_INFO system_info;
		GetSystemInfo(&system_info);
		size = system_info.dwPageSize;
	}
	return size;
}

uint64_t ArchUtils::GetTotalRam(void)
{
	uint64_t size;
	GetPhysicallyInstalledSystemMemory(&size);
	return size;
}

