#include "StackAllocator.h"
#include <stdint.h>
#include <memory>

namespace
{
	static const size_t SIZE_OF_ALLOCATION_OFFSET = sizeof(uint32_t);
	static_assert(SIZE_OF_ALLOCATION_OFFSET == 4, "Allocation offset has wrong size.");
	char* AlignTop(char* pointer, size_t alignment)
	{
		void* temp = pointer;
		std::align(alignment, 0, temp, alignment);
		return (char*)temp;
	}
}


void* StackAllocator::Allocate(size_t size, size_t alignment, size_t offset)
{
	size += SIZE_OF_ALLOCATION_OFFSET;
	offset += SIZE_OF_ALLOCATION_OFFSET;

	const uint32_t allocationOffset = static_cast<uint32_t>(m_current - m_start);

	m_current = AlignTop(m_current + offset, alignment) - offset;

	if (m_current + size > m_end)
	{
		return nullptr;
	}

	union
	{
		void* as_void;
		char* as_char;
		uint32_t* as_uint32_t;
	};

	as_char = m_current;
	*as_uint32_t = allocationOffset;
	as_char += SIZE_OF_ALLOCATION_OFFSET;

	void* userPtr = as_void;
	m_current += size;
	return userPtr;
}

#include <assert.h>

void StackAllocator::Free(void* ptr)
{
	assert(ptr != nullptr);

	union
	{
		void* as_void;
		char* as_char;
		uint32_t* as_uint32_t;
	};

	as_void = ptr;

	// grab the allocation offset from the 4 bytes right before the given pointer
	as_char -= SIZE_OF_ALLOCATION_OFFSET;
	const uint32_t allocationOffset = *as_uint32_t;

	m_current = m_start + allocationOffset;
}